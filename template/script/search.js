
 var pagesearch=1;
  function loadpageserch()
  {
    var input=$("#search input").val();
    $.ajax({
              url:`https://api.themoviedb.org/3/search/multi/?api_key=22c6f0f31e5f11d77270c1e41c4fcc71`,
              type:"GET",
              data:{query:input,page:pagesearch}
          })
          .then(function(result){
            hideloading();
            console.log(result);
            $(".main").empty();
            listresult=result.results;
            for (let i=0;i<listresult.length;i++)
            {
              
                //listresult[i].overview=listresult[i].overview.substring(0,200)+"...";
              if ($("select").val()==1)  
              {
              if (listresult[i].media_type=="movie")
                $(".main").append(` <div class="col-sm-6 card " style="width: 18rem;padding:0px" >
                                <div class="row ">
                                <img class="col-sm-6 card-img-top " src="https://image.tmdb.org/t/p/w500/${listresult[i].poster_path}" alt="Card image cap">
                                <div class="col-sm-6">
                                  <h5 class="card-title">${listresult[i].title}</h5>
                                  <p class="card-text">${listresult[i].overview}</p>
                                  <p>Rating: ${listresult[i].vote_average}</p>
                                  <p>Release: ${listresult[i].release_date}</p>
                                  <a  class="btn btn-primary movie" href="./DetailMovie.html?&${listresult[i].id}">Get Detail</a>
                                </div>
                              </div>
                      </div>`); 
              }
              else{
                if (listresult[i].media_type!="movie")
                {
                  $(".main").append(` <div class="col-sm-6 card " style="width: 18rem;padding:0px" >
                                <div class="row ">
                                <img class="col-sm-6 card-img-top " src="https://image.tmdb.org/t/p/w500/${listresult[i].profile_path}" alt="Card image cap">
                                <div class="col-sm-6">
                                  <h5 class="card-title">${listresult[i].name}</h5>
                                  <p class="card-text">Deparment: ${listresult[i].known_for_department}</p>
                                  <p>Rating: ${listresult[i].vote_average}</p>
                                  <a href="./Detailactor.html?&${listresult[i].id}"><button  class="btn btn-primary movie" >Get Detail</button><a>
                                </div>
                              </div>
                      </div>`); 
                }
              }
             
            }
            
          })
          displayloading();
  }
